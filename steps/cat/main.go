package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"gopkg.in/yaml.v3"
)

var (
	action = flag.String("action", "", "")
	inputs = flag.String("inputs", "", "")
)

func main() {
	flag.Parse()
	if *action == "" {
		panic("--action is required")
	}
	if *inputs == "" {
		panic("--inputs are required")
	}
	inputMap := map[string]string{}
	if err := json.Unmarshal([]byte(*inputs), &inputMap); err != nil {
		panic(err)
	}
	workflow := map[string]any{
		"jobs": map[string]any{
			"single-action": map[string]any{
				"runs-on": "ubuntu-latest",
				"steps": []map[string]any{{
					"uses": *action,
					"with": inputMap,
				}},
			},
		},
	}
	data, err := yaml.Marshal(workflow)
	if err != nil {
		panic(err)
	}
	if err := os.WriteFile("single-step.yml", data, 0640); err != nil {
		panic(err)
	}
	cmd := exec.Command(
		"act",
		"--workflows", "single-step.yml",
		"-j", "single-action",
		"-P", "catthehacker/ubuntu:act-latest",
	)
	out, err := cmd.CombinedOutput()
	if err != nil {
		panic(fmt.Sprintf("%v: %v", err, string(out)))
	}
	lines := strings.Split(string(out), "\n")
	outputFileContents := ""
	for _, line := range lines {
		_, setOutput, ok := strings.Cut(line, "::set-output::")
		if !ok {
			continue
		}
		setOutput = strings.TrimSpace(setOutput)
		k, v, ok := strings.Cut(setOutput, "=")
		if !ok {
			continue
		}
		output := k + "=" + v
		outputFileContents += output
		fmt.Println(output)
	}
	outputFilename := os.Getenv("STEP_RUNNER_OUTPUT")
	if outputFilename == "" {
		panic("STEP_RUNNER_OUTPUT not set")
	}
	err = os.WriteFile(outputFilename, []byte(outputFileContents), 0640)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(out))
}
