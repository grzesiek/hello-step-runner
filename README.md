Hacking around with [Step Runner](https://gitlab.com/gitlab-org/step-runner)

- Demo: https://youtu.be/TU_53vWVKeE
- Experiment feedback: https://gitlab.com/gitlab-org/step-runner/-/issues/10
- Blueprint: https://docs.gitlab.com/ee/architecture/blueprints/gitlab_steps/